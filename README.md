# ClickHelper

A WebExtension which aims to improve the experience of its users.

## Development

### Requirements

* npm
* Chromium

### First time setup

Install the development dependencies:

    npm ci

### Start the plugin

    npm start

