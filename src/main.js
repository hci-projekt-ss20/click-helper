"use strict";

const elementRegistry = new ClickableElementRegistry()

window.addEventListener("load", () => {
    updateMaxDistance()
    createStyles()
    elementRegistry.scanDom()
    elementRegistry.start()
});

window.addEventListener("resize", () => updateMaxDistance())

window.addEventListener("mousemove", event => {
    const mX = event.clientX
    const mY = event.clientY

    elementRegistry.pause()
    applyEffects(elementRegistry.getElements(), mX, mY)
    elementRegistry.start()
});
