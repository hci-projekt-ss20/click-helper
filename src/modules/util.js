"use strict";

function isImg(element) {
    return element.tagName === "IMG"
}

function hasImg(element) {
    let result = false

    for (let child of element.children) {
        if (isImg(child)) {
            result = true
            break
        }
    }

    return result
}
