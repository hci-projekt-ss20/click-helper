"use strict";

function hasOnClick(element) {
    return !!element.getAttribute('onclick')
}

function hasPointerAsCursor(element) {
    return window.getComputedStyle(element).getPropertyValue("cursor") === "pointer"
}

function isLink(element) {
    return !!element.getAttribute('href')
}

function isButton(element) {
    return element.tagName.toLowerCase() === "button"
}

function isClickable(element) {
    return element instanceof HTMLElement && (hasPointerAsCursor(element) || isLink(element) || isButton(element))
}
