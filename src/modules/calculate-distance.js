"use strict";

function calculateCenter(rect) {
    const centerX = rect.x + rect.width / 2
    const centerY = rect.y + rect.height / 2
    return { x: centerX, y: centerY }
}

function calculateDistance(x1, y1, x2, y2) {
    const a = Math.abs(x2 - x1)
    const b = Math.abs(y2 - y1)
    return Math.floor(Math.hypot(a, b))
}

function calculateDistanceToElement(boundingClientRect, mouseX, mouseY) {
    const center = calculateCenter(boundingClientRect)
    return calculateDistance(center.x, center.y, mouseX, mouseY)
}

function isVisible(boundingClientRect) {
    return (
        boundingClientRect.top >= 0 &&
        boundingClientRect.left >= 0 &&
        boundingClientRect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        boundingClientRect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
}
