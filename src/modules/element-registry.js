"use strict";

const observerConfig = {attributes: true, childList: true, subtree: true}

function ElementMetaData(element) {
    this.currentEffectLevel = 0
    this.hasImg = isImg(element) || hasImg(element)
}

/**
 * This is a registry which holds a collection of all clickable elements of a web site.
 * The elements are stored in a map where the HTMLElement is used as a key and some metadata as value
 * Changes to the web site after the initial load are detected with the MutationObserver API.
 *
 * Usage:
 * Call scanDom() to scan the whole DOM for clickable elements. Needs to be called one time once the page loaded.
 * To retrieve clickable elements use getElements(). The returned collection is mutable and you should not modify it.
 * Call start() to start listening to changes in the DOM.
 * Changes that the user performs on the DOM trigger the observer of this class.
 * Call pause() before performing any changes and call start() again afterwards.
 *
 */
class ClickableElementRegistry {
    clickableElements = new Map()

    observer = new MutationObserver(mutations => {
        for (let mutation of mutations) {
            if (mutation.type === 'childList') {

                for (let addedElement of mutation.addedNodes) {
                    if (isClickable(addedElement)) {
                        this.addClickableElement(addedElement)
                    }
                }

                for (let removedElement of mutation.removedNodes) {
                    this.removeClickableElement(removedElement);
                }

            } else if (mutation.type === 'attributes') {
                let element = mutation.target

                if (isClickable(element)) {
                    this.addClickableElement(element)
                } else {
                    this.removeClickableElement(element);
                }
            }
        }
    });

    start = () => this.observer.observe(document.body, observerConfig);
    pause = () => this.observer.disconnect();
    getElements = () => this.clickableElements;

    addClickableElement = element => {
        if (!this.clickableElements.has(element)) {
            const metadata = new ElementMetaData(element)
            initElement(element, metadata)
            this.clickableElements.set(element, metadata);
        }
    }

    removeClickableElement = element => {
        const metadata = this.clickableElements.get(element)
        const wasRemoved = this.clickableElements.delete(element);
        if (wasRemoved) {
            cleanupElement(element, metadata)
        }
    }

    scanDom = () => {
        this.clickableElements.clear()
        let elements = document.body.getElementsByTagName('*')
        for (let element of elements) {
            if (isClickable(element)) {
                this.addClickableElement(element)
            }
        }
    }
}
