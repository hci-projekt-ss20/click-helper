"use strict";

const CLASSNAME_PREFIX = "frauas-clickhelper-effect-"
const CLASSNAME_PREFIX_REVERSED = CLASSNAME_PREFIX + "reversed-"
const ATTR_NAME = CLASSNAME_PREFIX + "level"

const R = 228
const G = 112
const B = 104
const MAX_EFFECT_SPREAD = 20
let maxDistance = 800 //in px, 800 gets overwritten immediately on page load

function calculateClassPrefix(metaData) {
    return metaData.hasImg ? CLASSNAME_PREFIX_REVERSED : CLASSNAME_PREFIX
}

function calculateEffectLevel(distance) {
    // Maps distance to a number between [0, 100]
    if (distance >= maxDistance) {
        return 0
    }

    return 100 - Math.round(distance / maxDistance * 100)
}

function calculateScaledColor(effectIntensity) {
    return {
        r: 255 - Math.floor(effectIntensity * (255 - R)),
        g: 255 - Math.floor(effectIntensity * (255 - G)),
        b: 255 - Math.floor(effectIntensity * (255 - B))
    }
}

function applyEffects(elements, mouseX, mouseY) {
    // Updates to the DOM are batched to minimize the amount of repainting the browser has to do
    const classNameUpdates = []

    for (let [element, metadata] of elements) {
        // Retrieve it once at the beginning so it does not need to be recomputed
        let boundingClientRect = element.getBoundingClientRect();

        // Skip elements that are not visible
        if (!isVisible(boundingClientRect)) {
            continue
        }

        const distance = calculateDistanceToElement(boundingClientRect, mouseX, mouseY)
        const oldEffectLevel = metadata.currentEffectLevel
        const newEffectLevel = calculateEffectLevel(distance)
        const prefix = calculateClassPrefix(metadata)
        const oldEffectClass = prefix + oldEffectLevel
        const newEffectClass = prefix + newEffectLevel

        if (oldEffectLevel !== newEffectLevel) {
            // update effect
            metadata.currentEffectLevel = newEffectLevel
            classNameUpdates.push(() => element.className = element.className.replace(oldEffectClass, newEffectClass))
        }
    }

    classNameUpdates.forEach(f => f())
}

function createStyles() {
    const style = document.createElement("style")

    style.appendChild(document.createTextNode(`
        .${CLASSNAME_PREFIX + "0"} {
            box-shadow: none
       }
    `))

    for (let i = 1; i <= 100; i++) {
        const effectIntensity = i / 100.0
        const {r, g, b} = calculateScaledColor(effectIntensity)
        const color = `rgba(${r}, ${g}, ${b}, 0.5)`
        const spread = MAX_EFFECT_SPREAD * effectIntensity

        style.appendChild(document.createTextNode(`
            .${CLASSNAME_PREFIX + i} {
                box-shadow: inset 0px 0px 5px ${spread}px ${color};
            }
        `))

        style.appendChild(document.createTextNode(`
            .${CLASSNAME_PREFIX_REVERSED + i} {
                box-shadow: 0px 0px 5px ${spread}px ${color};
            }
        `))
    }

    document.head.appendChild(style)
}

function initElement(element, metaData) {
    const prefix = calculateClassPrefix(metaData)
    element.classList.add(prefix + metaData.currentEffectLevel)
}

function cleanupElement(element, metaData) {
    const prefix = calculateClassPrefix(metaData)
    element.classList.remove(prefix + metaData.currentEffectLevel)
}

function updateMaxDistance() {
    let distance = Math.hypot(document.documentElement.clientHeight, document.documentElement.clientWidth)
    distance = Math.round(distance / 2.0)
    maxDistance = distance
}
